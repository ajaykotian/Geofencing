package com.google.android.gms.location.sample.geofencing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ViewEntries extends AppCompatActivity {

    private TextView tvEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_entries);
        tvEntries = (TextView) findViewById(R.id.tv_entries);

        String entries = (String) MySharedPrefs.getInstance().get("ENTRIES","NO ENTRIES");

        tvEntries.setText(entries);

        entries.split(getString(R.string.separator));
    }
}
