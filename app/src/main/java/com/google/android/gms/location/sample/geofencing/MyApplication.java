package com.google.android.gms.location.sample.geofencing;

import android.app.Application;

/**
 * Created by ajaykotian on 22/9/17.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MySharedPrefs.initialize(this);
    }
}
