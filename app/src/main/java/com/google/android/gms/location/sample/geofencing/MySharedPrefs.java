package com.google.android.gms.location.sample.geofencing;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


/**
 * Created by Shraddha on 17/2/16.
 */
public class MySharedPrefs {

    private static final String SHARED_PREFS_NAME = MySharedPrefs.class.getSimpleName();
    private SharedPreferences sharedPreferences;
    private static MySharedPrefs mySharedPrefs;
    private SharedPreferences.Editor editor;

    public MySharedPrefs(Context appContext) {
        this.sharedPreferences = appContext.getSharedPreferences(SHARED_PREFS_NAME, Activity.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public static void initialize(Context context) {
        if(mySharedPrefs != null) {
            Log.i(SHARED_PREFS_NAME, "SharedPref already initialize");
            return;
        }
        mySharedPrefs = new MySharedPrefs(context);
    }

    public static MySharedPrefs getInstance() {
        return mySharedPrefs;
    }
    public void add(String key, Object value) {

        if (value instanceof String) {
            editor.putString(key, (String) value);
        }
        else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        }
        else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        }
        else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        }
        else {
            Log.e(SHARED_PREFS_NAME, "Value not inserted, Type " + value.getClass() + " not supported");
        }
        editor.commit();
    }

    public Object get(String key, Object defValue) {
        if (defValue instanceof String) {
            return sharedPreferences.getString(key, (String) defValue);
        }
        else if (defValue instanceof Boolean) {
            return sharedPreferences.getBoolean(key, (Boolean) defValue);
        }
        else if (defValue instanceof Integer) {
            return sharedPreferences.getInt(key, (Integer) defValue);
        }
        else if (defValue instanceof Long) {
            return sharedPreferences.getLong(key, (Long) defValue);
        }
        else {
            return null;
        }
    }
    public void clearSharePref() {
        editor.clear().commit();
    }

    public void clearSharePrefComplete() {
        editor.clear().commit();
    }
}