package com.google.android.gms.location.sample.geofencing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GpsLocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Date currentTime = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdf.setTimeZone(Calendar.getInstance().getTimeZone());

        String sCurrentTime = sdf.format(currentTime);

        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        String action = "";

        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            action += "GPS Enabled";
        } else {
            action += "GPS Disabled";
        }

        String existingEntries = (String) MySharedPrefs.getInstance().get("GPS_STATE", "");

        MySharedPrefs.getInstance().add("GPS_STATE", existingEntries+" # "+action+" $ "+sCurrentTime);

        Log.e("GPS_STATE",action);

    }
}